#!/usr/bin/env python
# coding: utf-8

# In[19]:


import sys
import os
from google.cloud import automl_v1beta1
from google.cloud.automl_v1beta1.proto import service_pb2



# In[11]:


def get_prediction(content, project_id, model_id):
    prediction_client = automl_v1beta1.PredictionServiceClient()
    name = 'projects/{}/locations/us-central1/models/{}'.format(project_id, model_id)
    payload = {'image': {'image_bytes': content }}
    params = {}
    request = prediction_client.predict(name, payload, params)
    return request  # waits till request is returned


# In[42]:


def main(folderpath):
    n = 0
    imageNameList = []
    labelList = {}
    for entry in os.scandir(path):
        if entry.is_file():
            n = n + 1
            imageNameList.append(entry.name)
    project_id = 'mytestproject-1487674567360'
    model_id = 'ICN8813463195576683326'
    
    for i in range(n):
        file_path = folderpath+imageNameList[i]
        with open(file_path, 'rb') as ff:
            content = ff.read()
        label = get_prediction(content, project_id,  model_id)
        item_dict={}
        item_dict['label'] = label.payload[0].display_name
        item_dict['score'] = label.payload[0].classification.score
        labelList[imageNameList[i]] = item_dict
    return labelList


if __name__ == "__main__":
    path = sys.argv[1]
    jsonKey = main(path)
    print(jsonKey)
    sys.stdout.flush()

